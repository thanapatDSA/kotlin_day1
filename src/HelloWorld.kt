import kotlin.math.max

fun main(args: Array<String>) {
    arrays()
    println(findMaxValue(arrayOf(5, 1, 2, 3, 4)))

}

fun helloWorld(): Unit {
    println("Hello, World! EiEi")
}

fun helloNore(text: String): Unit {
    println("Hello $text")
}

fun sureName(firstName: String = "annonymous", lastName: String = "annonymous"): Unit {
    println("$lastName $firstName")
}

fun gradeReport(firstName: String = "annonymous", lastName: String = "annonymousLastName", gpa: Double = 0.00): Unit =
    println("Mr. $firstName $lastName GPA: is $gpa")

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun resultGrade(socre: Int): String? {
    var grade: String? = getGrade(socre)
    if (grade == null) {
        return "null ja"
    } else {
        return "Your grade is $grade " + getAbbreviation(grade.toCharArray()[0])
    }
}

fun arrays(): Unit {
    var arrays = arrayOf(1, 3, 5, 7)
    for ((index, value) in arrays.withIndex()) {
        println("$index , value = $value")
    }
}

fun findMaxValue(values: Array<Int>): Int {
    var max: Int =  values[0]
    for ((index, value) in values.withIndex()) {
        if (values[index] > max ) {
            max = values[index]
        }
    }
    return max
}