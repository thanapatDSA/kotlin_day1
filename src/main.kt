abstract class Person(var name: String, var surnName: String, var gpa: Double){
    constructor(name: String,surnName: String) : this(name, surnName, 0.0){
    }
    abstract  fun goodBoy():Boolean
    open fun getDetails():String{
        return "$name $surnName has score $gpa"
    }
}

fun preintGoodBoy(boolean: Boolean):String{
    if (boolean == true){
        return "Bright non slap"
    }else {
        return "Bright slap!"
    }
}

class Student(name: String,surnName: String, gpa: Double, var department:String) : Person(name,surnName,gpa){
    override fun goodBoy(): Boolean {
        return  gpa > 2.0
    }

    override fun getDetails(): String {
        return "$name $surnName has socre $gpa and study in $department"
    }
}

fun main(args: Array<String>) {
//    val nol: Person = Person("Sonchi", "Pitak")
//    val nol2: Person = Person("Eiei","Okota?", 2.22)
//    val nol3: Person = Person("Kaguya","Madoga",4.00)
    val nol4: Student = Student("Amuro","Ray",1.55,"Gundam Pilot")

    println(nol4.getDetails())
    println(preintGoodBoy(nol4.goodBoy()))
}