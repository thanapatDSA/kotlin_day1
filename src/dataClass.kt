data class Teacher(var name:String,var courseName: String, var  shirtColor: Color)
enum class  Color{
    RED, GREEN, BLUE
}

fun main(args: Array<String>) {
    var adHoc = object {
        var x: Int = 0
        var y: Int = 1
    }
    println(adHoc)
    println(adHoc.x+adHoc.y)
    var teacher1: Teacher = Teacher("Eiei", "popTeamPipi", Color.RED)
    var teacher2: Teacher = Teacher("Aw", "pop",Color.GREEN)
    var teacher3: Teacher = Teacher("Pipimi", "bobMemeMimi",Color.BLUE)
    val (teacherName, teacherCourseName) = teacher3
    var teacher4= teacher1.copy(courseName = "wwwww")
    println("$teacherName $teacherCourseName")
    println(teacher1.component1())
    println(teacher1.component2())
    println(teacher1)
    println(teacher2)
    println(teacher4)
    println(teacher1.equals(teacher3))
}