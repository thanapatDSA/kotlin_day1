abstract class Animal(var noOflegs: Int, var food: String) {

    abstract fun getSound(): String
    open fun getDetails():String{
        return "Legs:$noOflegs Food:$food "
    }
}

class Dog(noOflegs: Int, food: String, var name: String) : Animal(noOflegs, food) {
    override fun getSound(): String {
        return "WoWo"
    }
    override fun getDetails(): String {
        return "Legs:$noOflegs Food:$food Name:$name"
    }
}

class Lion(noOflegs: Int, food: String) : Animal(noOflegs, food) {
    override fun getSound(): String {
        return "Gao"
    }
    override fun getDetails(): String {
        return "Legs:$noOflegs Food:$food"
    }
}

fun main(args: Array<String>) {
    val Dog1: Dog = Dog(4, "Dog food", "Toy")
    val Lion1: Lion = Lion(4, "Lion food")
    println(Dog1.getDetails())
    println("Sound:"+Dog1.getSound())
    println(Lion1.getDetails())
    println("Sound:"+Lion1.getSound())
}